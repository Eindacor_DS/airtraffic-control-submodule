import static main.java.com.airtrafficcontrol.cli.ReportingUtil.printQueue;
import static main.java.com.airtrafficcontrol.system.AircraftQueue.getAircraftQueue;
import static java.util.logging.Logger.getLogger;

import main.java.com.airtrafficcontrol.cli.AircraftQueueRequestFactory;
import main.java.com.airtrafficcontrol.system.request.AircraftQueueRequestProcessor;
import main.java.com.airtrafficcontrol.cli.UserInput;
import main.java.com.airtrafficcontrol.system.errorhandling.InvalidInputException;
import main.java.com.airtrafficcontrol.system.errorhandling.QueueInitializationException;
import org.apache.commons.cli.ParseException;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main
{
    private static final String EXIT = "exit";

    private static final Logger LOG = getLogger(Main.class.getName());

    public static void main(String[] args)
    {
        String input;
        Scanner scanner = new Scanner(System.in);

        while (!(input = scanner.nextLine()).equalsIgnoreCase(EXIT))
        {
            try
            {
                AircraftQueueRequestProcessor.aqmRequestProcess(AircraftQueueRequestFactory.getRequest(new UserInput(input)));
                printQueue(getAircraftQueue());
            }
            catch (InvalidInputException | QueueInitializationException | ParseException e)
            {
                LOG.log(Level.SEVERE,e.getMessage());
            }
            catch (Exception e)
            {
                LOG.log(Level.SEVERE, String.format("An unexpected error has occurred: %s%n", e.getMessage()));
            }
        }

        scanner.close();
    }
}

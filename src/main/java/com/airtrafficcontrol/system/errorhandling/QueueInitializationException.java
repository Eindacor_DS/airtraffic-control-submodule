package main.java.com.airtrafficcontrol.system.errorhandling;

/**
 * Thrown when a request is made (other than startup) before the queue has been initialized.
 */
public class QueueInitializationException extends Exception
{
    public QueueInitializationException(String message)
    {
        super(message);
    }
}

package main.java.com.airtrafficcontrol.system.errorhandling;

public class InvalidInputException extends Exception {

    public InvalidInputException(String message)
    {
        super(message);
    }
}

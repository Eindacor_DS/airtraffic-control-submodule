package main.java.com.airtrafficcontrol.system;

public class Aircraft
{
    private final AircraftSize aircraftSize;
    private final AircraftType aircraftType;

    public Aircraft(AircraftSize aircraftSize, AircraftType aircraftType)
    {
        this.aircraftSize = aircraftSize;
        this.aircraftType = aircraftType;
    }

    public AircraftSize getSize()
    {
        return aircraftSize;
    }

    public AircraftType getType()
    {
        return aircraftType;
    }
}

package main.java.com.airtrafficcontrol.system;

/**
 * Enumerated properties expected from user input.
 */
public enum  AircraftPropertyName
{
    SIZE, TYPE;
}

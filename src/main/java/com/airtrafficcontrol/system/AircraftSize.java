package main.java.com.airtrafficcontrol.system;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public enum AircraftSize
{
    LARGE(0), SMALL(1);

    private int priority;

    // Statically cache sizes for constant-time lookup from string value.
    private static final Map<String, AircraftSize> STRING_TO_SIZE_MAP = new HashMap<>(AircraftSize.values().length);

    static
    {
        for (AircraftSize aircraftSize : AircraftSize.values())
        {
            String lowerCase = aircraftSize.name().toLowerCase();
            STRING_TO_SIZE_MAP.put(lowerCase, aircraftSize);
        }
    }

    AircraftSize(int priority)
    {
        this.priority = priority;
    }

    public int getPriority()
    {
        return priority;
    }

    public static Optional<AircraftSize> getAircraftSize(String input)
    {
        return Optional.ofNullable(STRING_TO_SIZE_MAP.get(input.toLowerCase()));
    }
}

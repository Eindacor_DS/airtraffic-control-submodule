package main.java.com.airtrafficcontrol.system;

import main.java.com.airtrafficcontrol.system.errorhandling.QueueInitializationException;
import main.java.com.airtrafficcontrol.system.request.RequestType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.UUID;

/**
 * Queue wrapper, ensuring that the aircrafts are prioritized by {@link AircraftType}, {@link AircraftSize}, enqueue
 * time, and {@link UUID}.
 */
public class AircraftQueue
{
    /**
     * Comparator used by the wrapped queue to establish priority.
     */
    private static final Comparator<AircraftQueueElement> AIRCRAFT_QUEUE_ELEMENT_COMPARATOR = (first, second) -> {
        Aircraft firstAircraft = first.getAircraft();
        Aircraft secondAircraft = second.getAircraft();

        if (!firstAircraft.getType().equals(secondAircraft.getType())) {
            return firstAircraft.getType().getPriority() - secondAircraft.getType().getPriority();
        }

        if (!firstAircraft.getSize().equals(secondAircraft.getSize())) {
            return firstAircraft.getSize().getPriority() - secondAircraft.getSize().getPriority();
        }

        // If insertion time is identical, compare by UUID to maintain list order.
        if (first.getCreationTime().getTime() == second.getCreationTime().getTime()) {
            return first.getUuid().compareTo(second.getUuid());
        }

        return first.getCreationTime().getTime() < second.getCreationTime().getTime() ? -1 : 1;
    };

    // PriorityQueues throw an IllegalArgumentException if the initial size is less than 1;
    private static final int INITIAL_QUEUE_CAPACITY = 1;

    /**
     * {@link AircraftQueue} singleton.
     */
    private static AircraftQueue aircraftQueue;

    /**
     * Wrapped {@link PriorityQueue} of {@link AircraftQueueElement} objects.
     */
    private PriorityQueue<AircraftQueueElement> aircrafts;

    private AircraftQueue()
    {
        // restrict creation for queue singleton
    }

    /**
     * Initialize the wrapped {@link PriorityQueue} object, enabling the {@link AircraftQueue} functionality.
     */
    public void init()
    {
        if (aircrafts == null)
        {
            aircrafts = new PriorityQueue<>(INITIAL_QUEUE_CAPACITY, AIRCRAFT_QUEUE_ELEMENT_COMPARATOR);
        }
    }

    /**
     * Returns singleton {@link AircraftQueue} instance.
     *
     * @return
     */
    public static AircraftQueue getAircraftQueue()
    {
        if (aircraftQueue == null)
        {
            aircraftQueue = new AircraftQueue();
        }

        return aircraftQueue;
    }

    /**
     * Adds {@link AircraftQueueElement} to the queue based on the queue's comparator and returns UUID.
     *
     * @param aircraft
     * @return
     */
    public UUID enqueue(Aircraft aircraft) throws QueueInitializationException
    {
        assertQueueInitialization();
        AircraftQueueElement aircraftQueueElement = new AircraftQueueElement(aircraft);
        aircrafts.add(aircraftQueueElement);
        return aircraftQueueElement.getUuid();
    }

    /**
     * Removes {@link AircraftQueueElement} from the queue based on the queue's comparator.
     *
     * @throws QueueInitializationException
     */
    public void dequeue() throws QueueInitializationException
    {
        assertQueueInitialization();
        aircrafts.poll();
    }

    /**
     * Reveals next {@link AircraftQueueElement} to be removed from the queue.
     *
     * @return
     * @throws QueueInitializationException
     */
    public AircraftQueueElement peek() throws QueueInitializationException
    {
        assertQueueInitialization();
        return aircrafts.peek();
    }

    /**
     * Removes all {@link AircraftQueueElement} objects from the queue.
     *
     * @throws QueueInitializationException
     */
    public void clear() throws QueueInitializationException
    {
        assertQueueInitialization();
        aircrafts.clear();
    }

    /**
     * Returns list of aircrafts in queue to ensure the queue itself remains guarded.
     *
     * @return
     */
    public List<AircraftQueueElement> getAircrafts() throws QueueInitializationException
    {
        assertQueueInitialization();

        aircrafts.size();
        PriorityQueue<AircraftQueueElement> aircraftsCopy = new PriorityQueue<>(aircrafts);
        List<AircraftQueueElement> aircraftQueueElements = new ArrayList<>(aircrafts.size());
        for (int i=0; i<aircrafts.size(); i++)
        {
            aircraftQueueElements.add(aircraftsCopy.poll());
        }
        return Collections.unmodifiableList(aircraftQueueElements);
    }

    /**
     * Throws {@link QueueInitializationException} if the wrapped {@link PriorityQueue} has not yet been instantiated.
     *
     * @throws QueueInitializationException
     */
    private void assertQueueInitialization() throws QueueInitializationException
    {
        if (aircrafts == null)
        {
            throw new QueueInitializationException(String.format("the queue has not yet been initialized, try running the following request:\t%s", RequestType.STARTUP.name().toLowerCase()));
        }
    }
}
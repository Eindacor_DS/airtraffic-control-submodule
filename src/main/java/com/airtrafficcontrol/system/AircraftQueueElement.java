package main.java.com.airtrafficcontrol.system;

import java.util.Date;
import java.util.UUID;

/**
 * Wrapper class for {@link Aircraft} objects to be added to the {@link AircraftQueue}, including unique identifiers and
 * {@link Date} objects for queue management purposes.
 */
public class AircraftQueueElement
{
    private final Aircraft aircraft;

    /**
     * creationTime is used to identify the earliest {@link AircraftQueueElement} added to the {@link AircraftQueue}.
     */
    private final Date creationTime;

    /**
     * uuid is used to differentiate between {@link AircraftQueueElement} objects that have identical {@link Aircraft}
     * objects that were added at the same time.
     */
    private final UUID uuid;

    AircraftQueueElement(Aircraft aircraft)
    {
        this.aircraft = aircraft;
        this.creationTime = new Date();
        this.uuid = UUID.randomUUID();
    }

    public Aircraft getAircraft()
    {
        return aircraft;
    }

    public Date getCreationTime()
    {
        return creationTime;
    }

    public UUID getUuid()
    {
        return uuid;
    }
}
package main.java.com.airtrafficcontrol.system.request;

import main.java.com.airtrafficcontrol.system.errorhandling.QueueInitializationException;

import static main.java.com.airtrafficcontrol.system.AircraftQueue.getAircraftQueue;

/**
 * Processes provided queue requests.
 */
public class AircraftQueueRequestProcessor
{
    private AircraftQueueRequestProcessor()
    {
        // restrict object creation
    }

    public static void aqmRequestProcess(AircraftQueueRequestIF aircraftQueueRequestIF) throws QueueInitializationException
    {
        aircraftQueueRequestIF.execute(getAircraftQueue());
    }
}

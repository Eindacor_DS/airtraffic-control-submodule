package main.java.com.airtrafficcontrol.system.request;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public enum RequestType
{
    STARTUP, ENQUEUE, DEQUEUE;

    /**
     *  Statically cache types for constant-time lookup.
     */
    private static final Map<String, RequestType> STRING_TO_REQUEST_TYPE = new HashMap<>(RequestType.values().length);

    static
    {
        for (RequestType requestType : RequestType.values())
        {
            STRING_TO_REQUEST_TYPE.put(requestType.name().toLowerCase(), requestType);
        }
    }

    public static Optional<RequestType> getRequestType(String string)
    {
        return Optional.ofNullable(STRING_TO_REQUEST_TYPE.get(string.toLowerCase()));
    }
}

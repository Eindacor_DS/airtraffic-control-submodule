package main.java.com.airtrafficcontrol.system.request;

import main.java.com.airtrafficcontrol.system.AircraftQueue;
import main.java.com.airtrafficcontrol.system.errorhandling.QueueInitializationException;

public interface AircraftQueueRequestIF
{
    /**
     * Execute request behavior on the provided {@link AircraftQueue}.
     * @param aircraftQueue
     * @throws QueueInitializationException
     */
    void execute(AircraftQueue aircraftQueue) throws QueueInitializationException;
}

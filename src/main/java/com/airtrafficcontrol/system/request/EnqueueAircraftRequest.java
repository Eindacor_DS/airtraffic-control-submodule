package main.java.com.airtrafficcontrol.system.request;

import main.java.com.airtrafficcontrol.system.Aircraft;
import main.java.com.airtrafficcontrol.system.AircraftQueue;
import main.java.com.airtrafficcontrol.system.errorhandling.QueueInitializationException;

/**
 * Adds an {@link Aircraft} to the queue based on priority.
 */
public class EnqueueAircraftRequest implements AircraftQueueRequestIF
{
    private final Aircraft aircraft;

    public EnqueueAircraftRequest(Aircraft aircraft)
    {
        this.aircraft = aircraft;
    }

    @Override
    public void execute(AircraftQueue aircraftQueue) throws QueueInitializationException
    {
        aircraftQueue.enqueue(aircraft);
    }
}

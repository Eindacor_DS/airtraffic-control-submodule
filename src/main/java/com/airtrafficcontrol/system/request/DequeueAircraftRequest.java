package main.java.com.airtrafficcontrol.system.request;

import main.java.com.airtrafficcontrol.system.AircraftQueue;
import main.java.com.airtrafficcontrol.system.errorhandling.QueueInitializationException;

/**
 * Removes an {@link main.java.com.airtrafficcontrol.system.Aircraft} from the queue based on priority.
 */
public class DequeueAircraftRequest implements AircraftQueueRequestIF
{
    @Override
    public void execute(AircraftQueue aircraftQueue) throws QueueInitializationException
    {
        aircraftQueue.dequeue();
    }
}

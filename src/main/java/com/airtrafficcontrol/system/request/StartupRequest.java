package main.java.com.airtrafficcontrol.system.request;

import main.java.com.airtrafficcontrol.system.AircraftQueue;

/**
 * Initializes queue for future requests.
 */
public class StartupRequest implements AircraftQueueRequestIF
{
    @Override
    public void execute(AircraftQueue aircraftQueue)
    {
        aircraftQueue.init();
    }
}

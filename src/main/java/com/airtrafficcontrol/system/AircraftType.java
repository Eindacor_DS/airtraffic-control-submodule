package main.java.com.airtrafficcontrol.system;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public enum AircraftType
{
    PASSENGER(0), CARGO(1);

    private final int priority;

    // Statically cache types for constant-time lookup from string value.
    private static final Map<String, AircraftType> STRING_TO_TYPE_MAP = new HashMap<>(AircraftType.values().length);

    static
    {
        for (AircraftType aircraftType : AircraftType.values())
        {
            String lowerCase = aircraftType.name().toLowerCase();
            STRING_TO_TYPE_MAP.put(lowerCase, aircraftType);
        }
    }

    public static Optional<AircraftType> getAircraftType(String input)
    {
        return Optional.ofNullable(STRING_TO_TYPE_MAP.get(input.toLowerCase()));
    }

    AircraftType(int priority)
    {
        this.priority = priority;
    }

    public int getPriority()
    {
        return priority;
    }
}

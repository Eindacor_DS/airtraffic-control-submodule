package main.java.com.airtrafficcontrol.cli;

import static java.util.logging.Logger.getLogger;

import main.java.com.airtrafficcontrol.system.AircraftQueue;
import main.java.com.airtrafficcontrol.system.AircraftQueueElement;
import main.java.com.airtrafficcontrol.system.errorhandling.QueueInitializationException;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Utility class for logging queue contents.
 */
public class ReportingUtil
{
    /**
     * Abbreviates UUID for reporting
     */
    private static final int UUID_STRING_SIZE = 8;

    private static final Logger LOG = getLogger(ReportingUtil.class.toString());

    private ReportingUtil()
    {
        // restrict object construction
    }

    /**
     * Prints queue contents to console.
     *
     * @param aircraftQueue
     * @throws QueueInitializationException         Thrown if the provided queue has not yet been initialized with the
     *                                              {@code startup} request.
     */
    public static void printQueue(AircraftQueue aircraftQueue) throws QueueInitializationException
    {
        List<AircraftQueueElement> aircraftQueueElements = aircraftQueue.getAircrafts();

        StringBuilder messageBuilder = new StringBuilder("current queue...\n");

        if (aircraftQueueElements.isEmpty())
        {
            messageBuilder.append("the queue is currently empty\n");
        }
        else
        {
            for (AircraftQueueElement aircraftQueueElement : aircraftQueueElements)
            {
                messageBuilder.append(getQueueElementString(aircraftQueueElement)).append("\n");
            }
        }

        LOG.log(Level.INFO, messageBuilder.toString());
    }

    /**
     * Formats and prints {@link AircraftQueueElement} data.
     *
     * @param aircraftQueueElement
     * @return
     */
    private static String getQueueElementString(AircraftQueueElement aircraftQueueElement)
    {
        return String.format("%s: %s | %s, %s",
                aircraftQueueElement.getUuid().toString().substring(0, UUID_STRING_SIZE),
                aircraftQueueElement.getCreationTime().toString(),
                aircraftQueueElement.getAircraft().getSize(),
                aircraftQueueElement.getAircraft().getType()
        );
    }
}

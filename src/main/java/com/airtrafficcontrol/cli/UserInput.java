package main.java.com.airtrafficcontrol.cli;

import main.java.com.airtrafficcontrol.system.AircraftPropertyName;
import main.java.com.airtrafficcontrol.system.AircraftSize;
import main.java.com.airtrafficcontrol.system.AircraftType;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Wraps user input for parsing and request identification.
 */
public class UserInput
{
    private static final int EMPTY = 0;
    private final Map<String, String> inputOptions = new HashMap<>(EMPTY);
    private final String requestArg;

    /**
     * Options permitted for user entry.
     */
    private static final Options OPTIONS = new Options();

    static
    {
        String[] sizes = Stream.of(AircraftSize.values()).map(aircraftSize -> aircraftSize.name().toLowerCase()).toArray(String[]::new);
        OPTIONS.addOption("s", AircraftPropertyName.SIZE.name().toLowerCase(), true, String.format("aircraft size (%s)", String.join(", ", sizes)));

        String[] types = Stream.of(AircraftType.values()).map(aircraftType -> aircraftType.name().toLowerCase()).toArray(String[]::new);
        OPTIONS.addOption("t", AircraftPropertyName.TYPE.name().toLowerCase(), true, String.format("aircraft types (%s)", String.join(", ", types)));
    }

    /**
     * Parse user input and identify request/options passed.
     *
     * @param input
     * @throws ParseException
     */
    public UserInput(String input) throws ParseException
    {
        String[] splitInput = Stream.of(input.split(" ")).map(String::toLowerCase).toArray(String[]::new);
        CommandLineParser commandLineParser = new DefaultParser();
        CommandLine commandLine = commandLineParser.parse(OPTIONS, splitInput);

        for (Option option : commandLine.getOptions()) {
            inputOptions.put(option.getLongOpt(), option.getValue());
        }

        requestArg = commandLine.getArgs()[0];
    }

    public String getRequestArg()
    {
        return requestArg;
    }

    public Optional<String> getInputOption(String key)
    {
        return Optional.ofNullable(inputOptions.get(key.toLowerCase()));
    }
}

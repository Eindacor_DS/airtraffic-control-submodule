package main.java.com.airtrafficcontrol.cli;

import main.java.com.airtrafficcontrol.system.Aircraft;
import main.java.com.airtrafficcontrol.system.AircraftPropertyName;
import main.java.com.airtrafficcontrol.system.AircraftSize;
import main.java.com.airtrafficcontrol.system.AircraftType;
import main.java.com.airtrafficcontrol.system.errorhandling.InvalidInputException;
import main.java.com.airtrafficcontrol.system.request.AircraftQueueRequestIF;
import main.java.com.airtrafficcontrol.system.request.DequeueAircraftRequest;
import main.java.com.airtrafficcontrol.system.request.EnqueueAircraftRequest;
import main.java.com.airtrafficcontrol.system.request.RequestType;
import main.java.com.airtrafficcontrol.system.request.StartupRequest;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Generates {@link AircraftQueueRequestIF} implementations based on the provided {@link UserInput} object.
 */
public class AircraftQueueRequestFactory
{
    private AircraftQueueRequestFactory()
    {
        // restrict object construction
    }

    /**
     * Inspects {@link UserInput} object to determine the intended request type.
     *
     * @param userInput
     * @return
     * @throws InvalidInputException        Thrown if the request is not recognized by the system or if the behavior has
     *                                      not yet been implemented.
     */
    public static AircraftQueueRequestIF getRequest(UserInput userInput) throws InvalidInputException
    {
        String commandArg = userInput.getRequestArg();

        RequestType requestType = RequestType.getRequestType(commandArg).orElseThrow(
                () -> new InvalidInputException(String.format("unidentified request type: %s", commandArg))
        );

        switch(requestType)
        {
            case STARTUP:
                return new StartupRequest();

            case ENQUEUE:
                return new EnqueueAircraftRequest(getAircraft(userInput));

            case DEQUEUE:
                return new DequeueAircraftRequest();

            default:
                throw new InvalidInputException(String.format("the behavior for this request has not yet been implemented: %s", requestType.name()));

        }
    }

    /**
     * Fetches aircraft from {@link UserInput} object.
     *
     * @param userInput
     * @return
     * @throws InvalidInputException        Thrown if the size or type properties are omitted from the request.
     */
    private static Aircraft getAircraft(UserInput userInput) throws InvalidInputException
    {
        AircraftSize aircraftSize = getAircraftProperty(AircraftPropertyName.SIZE.name(), userInput, AircraftSize::getAircraftSize);
        AircraftType aircraftType = getAircraftProperty(AircraftPropertyName.TYPE.name(), userInput, AircraftType::getAircraftType);

        return new Aircraft(aircraftSize, aircraftType);
    }

    /**
     * Fetches aircraft property value from {@link UserInput} object.
     *
     * @param propertyName
     * @param userInput
     * @param typeIdentifier            Function used to identify the property value based on user input.
     * @param <T>
     * @return
     * @throws InvalidInputException    Thrown if the value retrieved is invalid or if the property was not provided in
     *                                  the command.
     */
    private static <T> T getAircraftProperty(String propertyName, UserInput userInput, Function<String, Optional<T>> typeIdentifier) throws InvalidInputException
    {
        Supplier<InvalidInputException> missingeExceptionSupplier = () -> new InvalidInputException(String.format("could not find mandatory property \"%s\"", propertyName));
        String typeInput = userInput.getInputOption(propertyName).orElseThrow(missingeExceptionSupplier);

        Supplier<InvalidInputException> invalidPropertyExceptionSupplier = () -> new InvalidInputException(String.format("invalid value for %s option: %s", propertyName, typeInput));
        return typeIdentifier.apply(typeInput).orElseThrow(invalidPropertyExceptionSupplier);
    }
}

package test.java.com.airtrafficcontrol.cli;

import static main.java.com.airtrafficcontrol.cli.AircraftQueueRequestFactory.getRequest;
import static org.testng.Assert.assertThrows;
import static org.testng.Assert.assertTrue;

import main.java.com.airtrafficcontrol.cli.UserInput;
import main.java.com.airtrafficcontrol.system.AircraftPropertyName;
import main.java.com.airtrafficcontrol.system.AircraftSize;
import main.java.com.airtrafficcontrol.system.AircraftType;
import main.java.com.airtrafficcontrol.system.errorhandling.InvalidInputException;
import main.java.com.airtrafficcontrol.system.request.RequestType;
import main.java.com.airtrafficcontrol.system.request.DequeueAircraftRequest;
import main.java.com.airtrafficcontrol.system.request.EnqueueAircraftRequest;
import main.java.com.airtrafficcontrol.system.request.StartupRequest;
import org.apache.commons.cli.ParseException;
import org.testng.annotations.Test;

public class AircraftQueueRequestIFFactoryTest
{
    private static final String INVALID_INPUT = "invalid_input";

    @Test (dependsOnGroups = {"userInput"})
    public void enqueueWithoutSizeFails() throws ParseException {
        UserInput userInput = new UserInput(String.format("%s -%s %s", RequestType.ENQUEUE.name(), AircraftPropertyName.TYPE.name(), AircraftType.CARGO.name()));
        assertThrows(InvalidInputException.class, () -> getRequest(userInput));
    }

    @Test (dependsOnGroups = {"userInput"})
    public void enqueueWithoutTypeFails() throws ParseException {
        UserInput userInput = new UserInput(String.format("%s -%s %s", RequestType.ENQUEUE.name(), AircraftPropertyName.SIZE.name(), AircraftSize.LARGE.name()));
        assertThrows(InvalidInputException.class, () -> getRequest(userInput));
    }

    @Test (dependsOnGroups = {"userInput"})
    public void invalidSizeFails() throws ParseException
    {
        UserInput userInput = new UserInput(
                String.format("%s -%s %s -%s %s",
                        RequestType.ENQUEUE.name(),
                        AircraftPropertyName.SIZE.name(),
                        INVALID_INPUT,
                        AircraftPropertyName.TYPE.name(),
                        AircraftType.CARGO.name()
                )
        );

        assertThrows(InvalidInputException.class, () -> getRequest(userInput));
    }

    @Test (dependsOnGroups = {"userInput"})
    public void invalidTypeFails() throws ParseException
    {
        UserInput userInput = new UserInput(
                String.format("%s -%s %s -%s %s",
                        RequestType.ENQUEUE.name(),
                        AircraftPropertyName.SIZE.name(),
                        AircraftSize.LARGE.name(),
                        AircraftPropertyName.TYPE.name(),
                        INVALID_INPUT
                )
        );

        assertThrows(InvalidInputException.class, () -> getRequest(userInput));
    }

    @Test (dependsOnGroups = {"userInput"})
    public void invalidRequestFails() throws ParseException
    {
        UserInput userInput = new UserInput(INVALID_INPUT);
        assertThrows(InvalidInputException.class, () -> getRequest(userInput));
    }

    @Test (dependsOnGroups = {"userInput"})
    public void enqueueRequestCreate() throws ParseException, InvalidInputException
    {
        UserInput userInput = new UserInput(
                String.format("%s -%s %s -%s %s",
                        RequestType.ENQUEUE.name(),
                        AircraftPropertyName.SIZE.name(),
                        AircraftSize.LARGE.name(),
                        AircraftPropertyName.TYPE.name(),
                        AircraftType.CARGO.name()
                )
        );

        assertTrue(getRequest(userInput) instanceof EnqueueAircraftRequest);
    }

    @Test (dependsOnGroups = {"userInput"})
    public void dequeueRequestCreate() throws ParseException, InvalidInputException
    {
        UserInput userInput = new UserInput(RequestType.DEQUEUE.name());
        assertTrue(getRequest(userInput) instanceof DequeueAircraftRequest);
    }

    @Test (dependsOnGroups = {"userInput"})
    public void startupRequestCreate() throws ParseException, InvalidInputException
    {
        UserInput userInput = new UserInput(RequestType.STARTUP.name());
        assertTrue(getRequest(userInput) instanceof StartupRequest);
    }
}

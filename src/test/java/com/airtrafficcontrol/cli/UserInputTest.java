package test.java.com.airtrafficcontrol.cli;

import static org.testng.Assert.assertEquals;

import main.java.com.airtrafficcontrol.cli.UserInput;
import main.java.com.airtrafficcontrol.system.AircraftPropertyName;
import main.java.com.airtrafficcontrol.system.AircraftSize;
import main.java.com.airtrafficcontrol.system.AircraftType;
import main.java.com.airtrafficcontrol.system.request.RequestType;
import org.apache.commons.cli.ParseException;
import org.testng.annotations.Test;

public class UserInputTest
{
    @Test (groups = "userInput")
    public void enqueueParseTest() throws ParseException
    {
        String size = AircraftSize.LARGE.name();
        String type = AircraftType.CARGO.name();
        UserInput userInput = new UserInput(String.format("%s -%s %s -%s %s",
                RequestType.ENQUEUE.name().toLowerCase(), AircraftPropertyName.SIZE.name(), size, AircraftPropertyName.TYPE.name(), type));
        assertEquals(userInput.getRequestArg(), RequestType.ENQUEUE.name().toLowerCase());

        assertEquals(userInput.getInputOption(AircraftPropertyName.SIZE.name().toLowerCase()).get(), size.toLowerCase());
        assertEquals(userInput.getInputOption(AircraftPropertyName.TYPE.name()).get(), type.toLowerCase());
    }
}

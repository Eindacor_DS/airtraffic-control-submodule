package test.java.com.airtrafficcontrol.system;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import main.java.com.airtrafficcontrol.system.Aircraft;
import main.java.com.airtrafficcontrol.system.AircraftQueue;
import main.java.com.airtrafficcontrol.system.AircraftSize;
import main.java.com.airtrafficcontrol.system.AircraftType;
import main.java.com.airtrafficcontrol.system.errorhandling.QueueInitializationException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class AircraftQueueTest
{
    private static final AircraftQueue AIRCRAFT_QUEUE = AircraftQueue.getAircraftQueue();
    private static final Random RANDOM = new Random();
    private static final int EMPTY_QUEUE = 0;
    private static final int CHRONOLOGICAL_TEST_COUNT = 10;

    @BeforeTest
    public static void clearQueue() throws QueueInitializationException
    {
        AIRCRAFT_QUEUE.init();
        AIRCRAFT_QUEUE.clear();
    }

    @Test
    public void aircraftQueueClears() throws QueueInitializationException
    {
        AIRCRAFT_QUEUE.enqueue(getRandomAircraft());
        assertTrue(AIRCRAFT_QUEUE.getAircrafts().size() > EMPTY_QUEUE);
        AIRCRAFT_QUEUE.clear();
        assertEquals(EMPTY_QUEUE, AIRCRAFT_QUEUE.getAircrafts().size());
    }

    private static Aircraft getRandomAircraft()
    {
        AircraftSize aircraftSize = RANDOM.nextBoolean() ? AircraftSize.SMALL : AircraftSize.LARGE;
        AircraftType aircraftType = RANDOM.nextBoolean() ? AircraftType.CARGO : AircraftType.PASSENGER;
        return new Aircraft(aircraftSize, aircraftType);
    }

    @Test (dependsOnMethods = {"aircraftQueueClears"})
    public void passengerCraftsHavePriorityOverCargoCrafts() throws QueueInitializationException
    {
        AIRCRAFT_QUEUE.enqueue(new Aircraft(AircraftSize.LARGE, AircraftType.CARGO));
        UUID passengerId = AIRCRAFT_QUEUE.enqueue(new Aircraft(AircraftSize.LARGE, AircraftType.PASSENGER));
        assertEquals(AIRCRAFT_QUEUE.peek().getUuid(), passengerId);
    }

    @Test (dependsOnMethods = {"aircraftQueueClears"})
    public void largeCraftsHavePriorityOverSmallCrafts() throws QueueInitializationException
    {
        AIRCRAFT_QUEUE.enqueue(new Aircraft(AircraftSize.SMALL, AircraftType.CARGO));
        UUID largeId = AIRCRAFT_QUEUE.enqueue(new Aircraft(AircraftSize.LARGE, AircraftType.CARGO));
        assertEquals(AIRCRAFT_QUEUE.peek().getUuid(), largeId);
    }

    @Test (dependsOnMethods = {"aircraftQueueClears"})
    public void earlyCraftsHavePriorityOverLaterCrafts() throws QueueInitializationException, InterruptedException
    {
        UUID firstCraft = AIRCRAFT_QUEUE.enqueue(new Aircraft(AircraftSize.SMALL, AircraftType.CARGO));

        for (int i=0; i<CHRONOLOGICAL_TEST_COUNT; i++)
        {
            TimeUnit.SECONDS.sleep(1);
            AIRCRAFT_QUEUE.enqueue(new Aircraft(AircraftSize.SMALL, AircraftType.CARGO));
            assertEquals(AIRCRAFT_QUEUE.peek().getUuid(), firstCraft);
        }
    }
}

Air Traffic Control Submodule Demo
======
This is a demo application of an air traffic control submodule. It includes the module's operative classes, namely `AircraftQueue`, and a package that manages the command line interface for the application. To run the application, navigate to the project root via command line, and enter the following:

`java -jar AirTrafficSubmodule.jar`

Once the application is running, you may enter the following requests:

___

`startup`

Initializes queue for other queue-related requests.
___

`enqueue -size <size> -type <type>`

Adds an aircraft to the queue with the provided properties. Requires `startup` request to be called first
	
-s, -size: aircraft size (large, small)	

-t, -type: aircraft types (passenger, cargo)
	
example: 

`enqueue -type cargo -size small`
___
	
`dequeue`

Removes an aircraft from the queue. Requires `startup` request to be called first.
___

`exit`

Exits the application.